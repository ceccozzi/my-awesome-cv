# Makefile twentyseconds cv

files_tex = $(wildcard CVs/*.tex)
files_pdf = $(wildcard CVs/*.pdf)
cv_name = "Francesco_Salemi_cv"
all: pdf
	@echo "Done!"
pdf: CVs/*.tex
	@echo "Building.... $^"
	@$(foreach var,$(files_tex),xelatex -interaction=nonstopmode -jobname=$(var:.tex=) '$(var)' 1>/dev/null;convert -density 200 $(var:.tex=.pdf) -quality 90 -colorspace RGB $(var:.tex=.png) 1>/dev/null;)
png: CVs/*.pdf	
	@echo "Building pngs..$^"
	@$(foreach var,$(files_pdf),pdftoppm $(var) $(var:.pdf=) -png -f 1 -singlefile 1>/dev/null;)
clean:
	@rm -f CVs/*.aux CVs/*.dvi CVs/*.log CVs/*.out CVs/*.pdf CVs/*.bak 
	@echo "Clean done.";
