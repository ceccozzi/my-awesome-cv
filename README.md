# My awesome cv
### A git project to store, manage & automatize the production of my english, french and german "Curricula Vitae" (CVs)

The project uses GitLab CI functionalities to compile my Curricula: basically starting from a $`\LaTeX{}`$ style, i.e. a customized version of [twentysecondcv.cls](twentysecondcv.cls) ([developped originally by Carmine Spagnuolo](https://github.com/spagnuolocarmine/TwentySecondsCurriculumVitae-LaTex) and further improved [here](https://github.com/opensorceror/Data-Engineer-Resume-LaTeX)) and some $`\LaTeX{}`$ source files containing my data in different languages (the files *.tex). Any change to the project triggers an automated pipeline which tries to compile the $`\LaTeX{}`$ source files with the above-mentioned style: the result of the last pipeline run is reported by the badge below (in the "Download" section). In case of success, PDF documents are produced as "artifacts" and can be downloaded directly: they are stored for a maximum time of 2 years or till they are replaced by newer versions (i.e. the products of a newer and succesfull pipeline run). Pipeline settings, as customary, are stored in the [.gitlab-ci.yml](.gitlab-ci.yml) file.  

## Preview 

#### CV (english)

[![Résumé](https://gitlab.com/ceccozzi/my-awesome-cv/-/jobs/artifacts/master/raw/CVs/cv_salemi_EN-E.png?job=pdf)](https://gitlab.com/ceccozzi/my-awesome-cv/-/jobs/artifacts/master/CVs/cv_salemi_EN-E.pdf)

## Download

CV pipeline Status: [![pipeline status](https://gitlab.com/ceccozzi/my-awesome-cv/badges/master/pipeline.svg)](https://gitlab.com/ceccozzi/my-awesome-cv/commits/master)

[Browse](https://gitlab.com/ceccozzi/my-awesome-cv/-/jobs/artifacts/master/browse?job=pdf) the Artifacts or download the [english](https://gitlab.com/ceccozzi/my-awesome-cv/-/jobs/artifacts/master/raw/CVs/cv_salemi_EN-E.pdf?job=pdf), the [french](https://gitlab.com/ceccozzi/my-awesome-cv/-/jobs/artifacts/master/raw/CVs/cv_salemi_FR-E.pdf?job=pdf) or the [german](https://gitlab.com/ceccozzi/my-awesome-cv/-/jobs/artifacts/master/raw/CVs/cv_salemi_DE-E.pdf?job=pd)(tentative) PDFs directly.
